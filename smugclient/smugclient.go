// smugclient is an implementation of a simple OAuth client to access Smugmug
// The OAuth token returned by the Smugmug API does not expire, so it is
// saved in a file to be reused later without having to do the OAuth dance
// repeatedly.
// Author: Harini Krishnamurthy
package smugclient

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"github.com/golang/glog"
	"github.com/gomodule/oauth1/oauth"
)

const (
	SmugAuthEndpoint = "https://api.smugmug.com/services/oauth/1.0a/authorize"
	SmugRequestTokenEndpoint = "https://api.smugmug.com/services/oauth/1.0a/getRequestToken"
	SmugAccessTokenEndpoint = "https://api.smugmug.com/services/oauth/1.0a/getAccessToken"
	SmugApiEndpoint = "https://api.smugmug.com/api/v2"
	SmugUploadEndpoint = "https://upload.smugmug.com"
	AlbumUri = "/api/v2/album"
)

// SmugmugClientConfig represents client config params.
type SmugmugClientConfig struct {
	// Dir when credentials are stored
	TokenDir string
	// API token file, relative to TokenDir
	ApiTokenFile string
	// Access token file, relative to TokenDir
	AccessTokenFile string
	// Auth endpoint URI.
	AuthEndpoint string
	// Request token endpoint URI.
	RequestTokenEndpoint string
	// Access token endpoint URI.
	AccessTokenEndpoint string
	// API endpoint URI.
	ApiEndpoint string
	// Upload endpoint URI.
	UploadEndpoint string
	// Album URI.
	AlbumUri string
}

type SmugmugAuthClient struct {
	// OAuthv1.0 client.
	oauthClient *oauth.Client

	// cached OAuth credentials for the session.
	cachedCreds *oauth.Credentials

	// http client
	httpClient *http.Client

	// client config
	config SmugmugClientConfig
}

// getToken reads credentials (key and secret) from a file.
// The credentials can be API key and secret or OAuth key and secret.
// It is assumed that the credentials are stored in JSON format
// Example:
// {
//    "Token": "faketoken"
//    "Secret": "reallylongsecret"
// }
func getToken(tokenFilePath string) (*oauth.Credentials, error) {
	f, err := os.Open(tokenFilePath)
	if err != nil {
		return nil, fmt.Errorf("Error %v opening file %s", err, tokenFilePath)
	}
	defer f.Close()

	// read file and unmarshal contents into oauth.Credentials
	tokens, err := ioutil.ReadAll(f)
	var creds oauth.Credentials
	if err := json.Unmarshal(tokens, &creds); err != nil {
		return nil, fmt.Errorf("Error %v getting oauth tokens", err)
	}
	return &creds, nil
}

// authorize does the OAuth dance and returns the OAuth credentials.
// 1. Request temporary credentials using the API key and secret.
// 2. Make an authorization request with the temporary credentials - this
// presents a URL where the user has to log in and retrieve a verification
// code.
// 3. Use the verification code that the user inputs to request an access token.
func (s *SmugmugAuthClient) authorize() (*oauth.Credentials, error) {

	// Request temporary credentials
	tempcreds, err := s.oauthClient.RequestTemporaryCredentials(nil, "oob", nil)
	if err != nil {
		return nil, fmt.Errorf("Error: %v getting temporary credentials", err)
	}

	// Get the authorization URL 
	params := url.Values{}
	params.Add("Permissions", "Modify")
	params.Add("Access", "Full")
	authURL :=  s.oauthClient.AuthorizationURL(tempcreds, params)
	fmt.Printf("Please go to url: %v and get a verification code.\n", authURL)

	// Read the verification code
	var code string
	fmt.Print("Enter verification code: ")
	if _, err := fmt.Scanln(&code); err != nil {
		return nil, fmt.Errorf("Error %v reading verification code\n", err)
	}

	// Request the oauth token
	creds, _, err := s.oauthClient.RequestToken(nil, tempcreds, code)
	if err != nil {
		return nil, err
	}
	return creds, err
}

// saveAccessToken saves the OAuth credentials to the accessTokenFile
// The credentials are saved as JSON.
func (s *SmugmugAuthClient) saveAccessToken() {
	credBytes, err := json.Marshal(s.cachedCreds)
	if err != nil {
		glog.Errorf("error %v marshaling token to JSON", err)
	} else {
		accessTokenPath := filepath.Join(s.config.TokenDir, s.config.AccessTokenFile)
		if err:= ioutil.WriteFile(accessTokenPath, credBytes, 0644); err != nil {
			glog.Errorf("error %v writing token to file.", err)
		}
	}
}

// GetUserInfo gets information about the currently authenticated user.
func (s *SmugmugAuthClient) GetUserInfo() error {
	userUri := s.config.ApiEndpoint + "!authuser"
	// get the response back in JSON.
	params := url.Values{}
	params.Add("_accept", "application/json")
	resp, err := s.Get(userUri, params, http.Header{})
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Error code: %v", resp.StatusCode)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("Error %v reading response body", err)
	}
	glog.Infof("Smugmug GetUserInfo response body: %v", string(body))
	return nil
}

// Upload uploads image bytes to the specified album.
func (s *SmugmugAuthClient) Upload(albumKey, filename, mimeType string, imgBytes []byte, caption string, keywords []string) error {
	// create HTTP header for the upload request. 
	header := http.Header{}
	uri := s.config.AlbumUri + "/" + albumKey
	header.Add("X-Smug-AlbumUri", uri)
	header.Add("X-Smug-ResponseType", "JSON")
	header.Add("X-Smug-Version", "v2")
	header.Add("Content-Type", mimeType)
	header.Add("X-Smug-FileName", filename)
	header.Add("X-Smug-Keywords", strings.Join(keywords, ","))
	header.Add("X-Smug-Caption", caption)

	md5bytes := md5.Sum(imgBytes)
	header.Add("Content-MD5", base64.StdEncoding.EncodeToString(md5bytes[:]))

	resp, err := s.Post(s.config.UploadEndpoint, imgBytes, header)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("error while uploading. got response code: %v", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	glog.Infof("Smugmug upload response body: %v\n", string(body))
	return nil
}

// Post sends a HTTP POST request with the specified payload.
func (s *SmugmugAuthClient) Post(uri string, payload []byte, header http.Header) (*http.Response, error) {

	// create a new HTTP request and set the header.
	req, err := http.NewRequest("POST", uri, bytes.NewReader(payload))
	if err != nil {
		return nil, err
	}
	if err := s.setAuthHeader(uri, "POST", header, url.Values{}); err != nil {
		return nil, err
	}
	req.Header = header

	glog.Infof("Smugmug POST req: %v", req)
	// make the request and parse the response.
	return s.httpClient.Do(req)
}

// Get performs a HTTP GET request.
func (s *SmugmugAuthClient) Get(uri string, params url.Values, header http.Header) (*http.Response, error) {
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, err
	}
	if err := s.setAuthHeader(uri, "GET", header, params); err != nil {
		return nil, err
	}
	req.Header = header
	req.URL.RawQuery = params.Encode()
	
	return s.httpClient.Do(req)
}

// setAuthHeader sets the Authorization header
func (s *SmugmugAuthClient) setAuthHeader(uri, method string, header http.Header, params url.Values) error {
	// Set the Authorization header.
	ep, err := url.Parse(uri)
	if err != nil {
		return err
	}

	if err := s.oauthClient.SetAuthorizationHeader(header, s.cachedCreds, method, ep, params); err != nil {
		return err
	}
	return nil
}

// updateAccessToken gets new OAuth credentials or retrieves already
// existing credentials.
func (s *SmugmugAuthClient) updateAccessToken() error {
	if s.cachedCreds == nil {
		// If the OAuth token is not saved in the accessTokenFile, make
		// the auth request again and save the token.
		accessTokenPath := filepath.Join(s.config.TokenDir, s.config.AccessTokenFile)
		if _, err := os.Stat(accessTokenPath); os.IsNotExist(err) {
			creds, err := s.authorize()
			if err != nil {
				return err
			}
			s.cachedCreds = creds
			s.saveAccessToken()
		} else {
			// retrieve the token from the file and cache it.
			creds, err := getToken(accessTokenPath)
			if err != nil {
				return err
			}
			s.cachedCreds = creds
		}
	}
	return nil
}

// NewSmugmugAuthClient creates a new client with a valid OAuth token.
func NewSmugmugAuthClient(smugcfg SmugmugClientConfig) (*SmugmugAuthClient, error) {
	// get API key and secret.
	apicreds, err := getToken(filepath.Join(smugcfg.TokenDir, smugcfg.ApiTokenFile))
	if err != nil {
		return nil, err
	}
	// init an oauth client
	oauthClient := oauth.Client{
		TemporaryCredentialRequestURI: smugcfg.RequestTokenEndpoint,
		ResourceOwnerAuthorizationURI: smugcfg.AuthEndpoint,
		TokenRequestURI: smugcfg.AccessTokenEndpoint,
		Credentials: *apicreds,
	}
	s := &SmugmugAuthClient{
		oauthClient: &oauthClient,
		httpClient: http.DefaultClient,
		config: smugcfg,
	}
	// update the access token
	if err := s.updateAccessToken(); err != nil {
		return nil, err
	}
	return s, nil
}


