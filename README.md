This is a tool to import media from Instagram to Smugmug. It can use the 
downloaded Instagram data file as well as the Instagram GetRecentMedia API
to retrieve media from Instagram.