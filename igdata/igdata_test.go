// Unit tests for package igdata
package igdata

import(
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestToIGMetadata(t *testing.T) {
	tests := []struct{
		label string
		d DownloadMediaJSON
		mtype MediaType
		expectedIGdata IGMetadata
		err bool
	}{
		{
			"Valid JSON",
			DownloadMediaJSON{
				"Hello World #sample #hashtag",
				"2018-12-31T11:03:00",
				"/path/to/file",
			},
			PhotoType,
			IGMetadata{
				[]string{"sample", "hashtag"},
				"/path/to/file",
				"2018-12-31T19:03:00Z",
				PhotoType,
			},
			false,
		},
		{
			"Unexpected time format",
			DownloadMediaJSON{
				"Hello World #sample #hashtag",
				"1547060000",   // unexpected time format
				"/path/to/file",
			},
			PhotoType,
			IGMetadata{},
			true,

		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.label, func(t *testing.T) {
			igmd, err := toIGMetadata(test.d, test.mtype)
			if err != nil {
				if !test.err {
					t.Errorf("toIGMetadata: unexpected err: %v", err)
				}
			} 
			assert.Equal(t, test.expectedIGdata, igmd)
		})
	}
}

func TestHashtagsFromCaption(t *testing.T) {
	tests := []struct{
		label string
		caption string
		expectedHashtags []string
	}{
		{
			"All hashtags",
			"#this #caption #has #all #hashtags",
			[]string{"this", "caption", "has", "all", "hashtags"},
		},
		{
			"No hashtags",
			"Caption has no hashtags",
			[]string{},
		},
		{
			"Hashtags in the middle",
			"there are\n#hashtags #in #the #middle",
			[]string{"hashtags", "in", "the", "middle"},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.label, func (t *testing.T) {
			ht := hashtagsFromCaption(test.caption)
			assert.Equal(t, test.expectedHashtags, ht)
		})
	}
}

func TestFetchFile(t *testing.T) {
	tempfile, err := ioutil.TempFile("", "testfile")
	if err != nil {
		t.Errorf("error: %v creating tempfile", err)
	}
	defer os.Remove(tempfile.Name())

	data := []byte("test data")
	if _, err := tempfile.Write(data); err != nil {
		t.Errorf("error: %v writing data to tempfile", err)
	}
	if err := tempfile.Close(); err != nil {
		t.Errorf("error: %v closing tempfile", err)
	}

	tests := []struct{
		label string
		filename string
		expectedData []byte
		err bool
	}{
		{
			"Valid file location",
			tempfile.Name(),
			data,
			false,
		},
		{
			"Invalid file location",
			"file.doesnotexist",
			[]byte{},
			true,
		},
	}
	igdd := IGDownloadData{"", nil}
	for _, test := range tests {
		test := test
		t.Run(test.label, func(t *testing.T) {
			read, err := igdd.Fetch(test.filename)
			if err != nil {
				if !test.err {
					t.Errorf("expected error fetching data from file: %v", test.filename)
				}
			}
			assert.Equal(t, test.expectedData, read)
		})
	}
}

func TestFetchUrl(t *testing.T) {
	tests := []struct{
		label string
		mediaUrl string
		bytesExpected bool
		err bool
	}{
		{"Valid URL", "https://www.google.com", true, false},
		{"Invalid URL", "http://nosuch.url", false, true},
	}

	igrd := IGRecentData{}
	for _, test := range tests {
		test := test
		t.Run(test.label, func(t *testing.T) {
			data, err := igrd.Fetch(test.mediaUrl)
			if err != nil { 
				if !test.err {
					t.Errorf("got error: %v, no error expected for url: %v", err, test.mediaUrl)
				}
			}
			if len(data) > 0 && !test.bytesExpected {
				t.Errorf("error: %v. No bytes expected for url: %v", err, test.mediaUrl)
			}
		})
	}
}

func TestDownloadMediaIter(t *testing.T) {
	tests := []struct{
		label string
		numPhotos int
		numVideos int
	}{
		{"One Photo, One Video", 1, 1},
		{"One Video", 0, 1},
		{"One Photo", 1, 0},
	}
	for _, test := range tests {
		test := test
		t.Run(test.label, func(t *testing.T) {
			downloadDir, err := ioutil.TempDir("/var/tmp", "test")
			if err != nil {
				t.Errorf("error: %v creating temp dir", err)
			}
			defer os.RemoveAll(downloadDir)
			data, err := createDownloadData(test.numPhotos, test.numVideos, downloadDir, false)
			if err != nil {
				t.Errorf("error: %v creating download data", err)
			}
			igdd := &IGDownloadData{
				data: data,
			}

			actualNumPhotos := 0
			actualNumVideos := 0
			for m := range igdd.MediaIter() {
				if m.Type == PhotoType {
					actualNumPhotos++
				} else {
					actualNumVideos++
				}
			}
			assert.Equal(t, test.numPhotos, actualNumPhotos)
			assert.Equal(t, test.numVideos, actualNumVideos)
		})		
	}
}

func TestRecentMediaIter(t *testing.T) {
	tests := []struct{
		label string
		posts []struct{
			numPhotos int
			numVideos int
		}
	}{
		{"One post, only photo", []struct{numPhotos, numVideos int}{{1, 0}}},
		{"One post, only video", []struct{numPhotos, numVideos int}{{0, 1}}},
		{"One post, photo and video", []struct{numPhotos, numVideos int}{{1, 1} }},
		{"Two posts, one photo, one video", []struct{numPhotos, numVideos int}{{1, 0}, {0, 1} }},
	}
	for _, test := range tests {
		test := test
		t.Run(test.label, func(t *testing.T) {
			data := []RecentMediaRecordJSON{}
			totalExpPhotos := 0
			totalExpVideos := 0
			for i := 0; i < len(test.posts); i++ {
				p := test.posts[i]
				d := createRecentMediaRecordJSON(p.numPhotos, p.numVideos)
				totalExpPhotos += p.numPhotos
				totalExpVideos += p.numVideos
				data = append(data, d)
			}
			igrd := &IGRecentData{data}
			actualNumPhotos := 0
			actualNumVideos := 0
			for m := range igrd.MediaIter() {
				if m.Type == PhotoType {
					actualNumPhotos++
				} else {
					actualNumVideos++
				}
			}
			assert.Equal(t, totalExpPhotos, actualNumPhotos)
			assert.Equal(t, totalExpVideos, actualNumVideos)
		})
	}
}

func TestImgIterator(t *testing.T) {
	tests := []struct{
		label string
		numPhotos int
		numVideos int
		expectedTotal int
		err bool
	}{
		{"Valid media", 1, 1, 2, false},
		{"Invalid media", 1, 0, 0, true},
	}
	for _, test := range tests {
		test := test
		t.Run(test.label, func(t *testing.T) {
			downloadDir, err := ioutil.TempDir("/var/tmp", "test")
			if err != nil {
				t.Errorf("error: %v creating temp dir", err)
			}
			defer os.RemoveAll(downloadDir)
			data, err := createDownloadData(test.numPhotos, test.numVideos, downloadDir, !test.err)
			if err != nil {
				t.Errorf("error: %v creating download data", err)
			}
			igdd := &IGDownloadData{
				data: data,
			}
			actualNumPhotos := 0
			actualNumVideos := 0
			for idr := range ImageIterator(igdd) {
				if idr.Data.MimeType == "image/jpeg" {
					actualNumPhotos++
				} else if idr.Data.MimeType == "video/mp4" {
					actualNumVideos++
				}
				actualErr := (idr.Err != nil)
				assert.Equal(t, test.err, actualErr)
			}
			assert.Equal(t, test.expectedTotal, actualNumPhotos+actualNumVideos)
		})
	}
}

func createDownloadData(numPhotos, numVideos int, path string, writeBytes bool) (*DownloadRecordJSON, error) {
	photos := []DownloadMediaJSON{}
	for i := 0; i < numPhotos; i++ {
		fname := strconv.Itoa(i) + ".jpg"
		imgFilepath := filepath.Join(path, fname)
		photo := DownloadMediaJSON{
			Caption: "#hello #world",
			TakenAt: "2018-12-31T11:03:00",
			Path: imgFilepath,
		}
		photos = append(photos, photo)
		if writeBytes {
			content := []byte("fake image")
			if err := ioutil.WriteFile(imgFilepath, content, 0600); err != nil {
				return nil, err
			}
		}
	}
	videos := []DownloadMediaJSON {}
	for i := 0; i < numVideos; i++ {
		fname := strconv.Itoa(i) + ".mp4"
		videoFilepath := filepath.Join(path, fname)
		video := DownloadMediaJSON {
			Caption: "#hello #world #video",
			TakenAt: "2018-12-31T21:09:00",
			Path: videoFilepath,
		}
		videos = append(videos, video)
		if writeBytes {
			content := []byte("fake video")
			if err := ioutil.WriteFile(videoFilepath, content, 0600); err != nil {
				return nil, err
			}
		}
	}
	return &DownloadRecordJSON{
		Photos: photos,
		Videos: videos,
	}, nil
}

func createRecentMediaRecordJSON(numPhotos, numVideos int) RecentMediaRecordJSON {
	d := RecentMediaRecordJSON{}
	d.TakenAt = "1234567890"
	d.Tags = []string{"hello", "world"}
	if (numPhotos + numVideos > 1) {
		// populate multiple media
		mult := []RecentMediaJSON{}
		for i := 0; i < numPhotos; i++ {
			mult = append(mult, RecentMediaJSON{Image: getImage(i)})
		}
		for i := 0; i < numVideos; i++ {
			mult = append(mult, RecentMediaJSON{Video: getVideo(i)})
		}
		
		d.MultImages = mult
	} else if numPhotos > 0 {
		d.Image = getImage(0)
	} else if numVideos > 0 {
		d.Video = getVideo(0)
	}
	
	return d
}

func getImage(i int) StdResImageJSON {
	return StdResImageJSON{
		StdResImg: ImageUriJSON{"http://" + string(i) + ".photo.url"},
	}
}

func getVideo(i int) StdResImageJSON {
	return StdResImageJSON{
		StdResImg: ImageUriJSON{"http://" + string(i) + ".video.url"},
	}
}
