// package igdata supports reading Instagram data from 2 sources
// 1. Downloaded data (emailed in a zip file).
// 2. Instagram API, specfically /users/self/media/recent.
// Author: Harini Krishnamurthy
package igdata

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"strconv"
	"sync"
	"time"
	"github.com/golang/glog"
	"gitlab.com/harinik/instasmug/igclient"
)

type MediaType int

const (
	TimeLayout = "2006-01-02T15:04:05Z"

	PhotoType MediaType = iota
	VideoType
)

// The following fields correspond to the fields in the JSON representation
// of the data download from Instagram.
// It looks something like this:
// {"photos":
//   [{"caption": "Hand painted cyanotype on canvas paper\n#cyanotype #acrylic #paintedphotograph #handcoated #handcolored #alternativeprocess #digitalnegatives #analog #mixedmedia #artproject",
//     "taken_at": "2018-10-22T14:32:00",
//     "path": "photos/201810/fbd1741f902e6a282c7005e0af3d61d5.jpg"},
//    ...
//   ]
// }

// DownloadMediaJSON is a struct corresponding to Instagram's representation
// of a photo or video (downloaded IG data).
type DownloadMediaJSON struct {
	// note: these variables have to be exported, otherwise json.Unmarshal
	// does not work.
	Caption string `json:"caption"`
	TakenAt string `json:"taken_at"`
	Path string `json:"path"`
}

// DownloadRecordJSON contains a list of Photos and Videos.
type DownloadRecordJSON struct {
	Photos []DownloadMediaJSON `json:"photos"`
	Videos []DownloadMediaJSON `json:"videos"`
}

// IGDownloadData represents downloaded Instagram data.
// It implements the IGMediaFetcher interface.
type IGDownloadData struct {
	// base directory where the data is stored.
	baseIGDir string
	data *DownloadRecordJSON
}

// The following fields correspond to fields in the JSON response to
// Instagram's /users/self/media/recent endpoint.
// It looks something like this:
// {
//   "data": [{
//         "images" : { # corresponds to cover image if there are multiple.
//           "standard_resolution": {
//             "url": "https://scontent.cdninstagram.com/vp/foo.jpg"
//           }}
//         "created_time": "1546367890"
//         "tags": ["tag1", "tag2"],
//         "carousel_media": [{ # for multiple images
//             "images": {
//             ...
//             }
//           },
//           {
//             "images": {
//             ...
//             }
//           }]          
//       }]
//  }
// 
// Note that this JSON representation is very different from that for the
// downloaded IG data above.
// URL for the media.
type ImageUriJSON struct {
	ImgUri string `json:"url"`
}

// Standard resolution image
type StdResImageJSON struct {
	StdResImg ImageUriJSON `json:"standard_resolution"`
}

// Std resolution Photos and Videos 
type RecentMediaJSON struct {
	Image StdResImageJSON `json:"images"`
	Video StdResImageJSON `json:"videos"`
}

// A record corresponding to a single IG post.
type RecentMediaRecordJSON struct {
	Image StdResImageJSON `json:"images"`
	Video StdResImageJSON `json:"videos"`
	Tags []string `json:"tags"`
	TakenAt string `json:"created_time"`
	MultImages []RecentMediaJSON `json:"carousel_media"`
}

// Recent data for the user.
// It implements the IGMediaFetcher interface.
type IGRecentData struct {
	Data []RecentMediaRecordJSON `json:"data"`
}

// Instagram media fetcher interface.
type IGMediaFetcher interface {
	// Fetch fetches the media bytes from the specified location.
	// e.g. file or URL.
	Fetch(location string) ([]byte, error)
	// MediaIter returns a channel and populates it with a common
	// representation of the media.
	MediaIter() (<-chan IGMetadata)
}

// IGMetadata is a common representation of the metadata for IG media.
type IGMetadata struct {
	Tags []string
	Location string
	CreationTime string
	Type MediaType
}

// Parsed media data that includes the content bytes of the media.
type ParsedMediaData struct {
	Id string
	Hashtags []string
	Contents []byte
	Date string
	MimeType string
}

// Image data to put on the channel for the reader.
type ImgDataResult struct {
	Data *ParsedMediaData
	Err error
}

// load reads the Instagram metadata that is available as JSON
// from the specified file and returns a struct that represents that data.
func (igdl *IGDownloadData) load(fpath string) error {
	f, err := os.Open(fpath)
	defer f.Close()

	if err != nil {
		return fmt.Errorf("error: %s opening file: %s", err, fpath)
	}

	// read the contents and unmarshal the JSON into the provided
	// data structure.
	contents, err := ioutil.ReadAll(f)
	if err != nil {
		return fmt.Errorf("error: %s reading json file: %s", err, fpath)
	}
	
	recs := new(DownloadRecordJSON)
	if err := json.Unmarshal(contents, recs); err != nil {
		return fmt.Errorf("error: %s unmarshalling json file: %s", err, fpath)
	}
	igdl.data = recs
	return nil
}

// toIGMetadata converts from the JSON representation of downloaded IG data
// to a common format that encapsulates the metadata.
func toIGMetadata(p DownloadMediaJSON, mType MediaType) (IGMetadata, error) {
	// !!! HACK ALERT !!!
	// The TakenAt field in the Instagram data does not specify a timezone.
	// On manual inspection, it *appears* to be Pacific Time. So, append
	// -08:00 to the timestamp so that we can keep the times consistent with
	// the other source (API).
	p.TakenAt = p.TakenAt + "-08:00"
	ct, err := time.Parse(time.RFC3339, p.TakenAt)
	if err != nil {
		return IGMetadata{}, err
	}
	// !!! END HACK !!!
	return IGMetadata{
		Tags: hashtagsFromCaption(p.Caption),
		Location: p.Path,
		Type: mType,
		CreationTime: ct.UTC().Format(TimeLayout),
	}, nil
}

// hashtagsFromCaption extracts the hashtags that are present in the caption
// fields and returns a list of them. The # in the hashtags are stripped.
func hashtagsFromCaption(caption string) []string {
	fields := strings.Fields(caption)
	hashtags := []string{}
	for _, f := range(fields) {
		hashIdx := strings.LastIndex(f, "#")
		if  hashIdx >= 0 {
			f = f[hashIdx+1:]
			hashtags = append(hashtags, f)
		}
	}
	return hashtags
}

// MediaIter iterates through the Photos and Videos data structures,
// populates and returns a channel.
func (igdl *IGDownloadData) MediaIter() (<-chan IGMetadata) {
	ch := make(chan IGMetadata)
	go func() {
		for _, p := range igdl.data.Photos {
			igmd, err := toIGMetadata(p, PhotoType)
			if err != nil {
				glog.Errorf("error: %v getting IGMetadata for photo taken at: %v. Ignoring.", err, p.TakenAt)
				continue
			}
			ch <- igmd
		}
		for _, p := range igdl.data.Videos {
			igmd, err := toIGMetadata(p, VideoType)
			if err != nil {
				glog.Errorf("error: %v getting IGMetadata for photo taken at: %v. Ignoring.", err, p.TakenAt)
				continue
			}
			ch <- igmd
		}
		close(ch)
	}()
	return ch
}

// Fetch fetches the contents of the file specified.
func (igdl *IGDownloadData) Fetch(fname string) ([]byte, error) {
	f, err := os.Open(filepath.Join(igdl.baseIGDir, fname))
	empty := make([]byte, 0)
	if err != nil {
		return empty, err
	}
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return empty, err
	}
	return data, nil
}

// MediaIter populates and returns a channel of IG metadata, from recent
// data retrieved from the IG API.
func (igrd *IGRecentData) MediaIter() (<-chan IGMetadata) {
	ch := make(chan IGMetadata)
	go func() {
		for _, d := range igrd.Data {
			// format the time to a consistent format.
			// first, convert the UNIX time string into an int64.
			i, err := strconv.ParseInt(d.TakenAt, 10, 64)
			if err != nil {
				glog.Infof("unable to convert time: %v to int64. Ignoring record.", d.TakenAt)
				continue
			}
			// format into the layout.
			ct := time.Unix(i, 0).Format(TimeLayout)
			// TODO: This code is a bit ugly because of the way the JSON
			// is structured and unmarshaled. Figure out a way to get rid
			// of duplication here.
			if len(d.MultImages) > 0 {
				for _, m := range d.MultImages {
					md := IGMetadata{
						Tags: d.Tags,
						CreationTime: ct,
					}
					md.Location = m.Image.StdResImg.ImgUri
					md.Type = PhotoType
					if len(m.Video.StdResImg.ImgUri) > 0 {
						md.Location = m.Video.StdResImg.ImgUri
						md.Type = VideoType
					}			
					ch <- md
				}
			} else {
				md := IGMetadata{
					Tags: d.Tags,
					CreationTime: ct,
				}
				md.Location = d.Image.StdResImg.ImgUri
				md.Type = PhotoType
				if len(d.Video.StdResImg.ImgUri) > 0 {
					md.Location = d.Video.StdResImg.ImgUri
					md.Type = VideoType
				}			
				ch <- md
			}
		}
		close(ch)
	}()
	return ch
}

// Fetch fetches the contents of the specified url.
func (igrd *IGRecentData) Fetch(mediaUri string) ([]byte, error) {
	resp, err := http.Get(mediaUri)
	if err != nil {
		return []byte{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, err
	}
	return body, nil
}

// ImageIterator reads media info off the channel populated by MediaIter(),
// reads the image bytes, populates and returns a channel.
func ImageIterator(igf IGMediaFetcher) (<-chan ImgDataResult) {
	ch := make(chan ImgDataResult)
	go func() {
		var wg sync.WaitGroup
		for p := range igf.MediaIter() {
			// create a goroutine for each Photo, so they can be read
			// concurrently.
			wg.Add(1)
			go func(p IGMetadata) {
				defer wg.Done()
				result := ImgDataResult{Err: nil}
				img, err := igf.Fetch(p.Location)
				if err != nil {
					result.Data = &ParsedMediaData{"", []string{}, []byte{}, "", ""}
					result.Err = err
				} else {
					// generate an ID
					id := md5.Sum(append([]byte(p.CreationTime), img...))
					idStr := hex.EncodeToString(id[:])
					mimeType := "image/jpeg"
					if p.Type == VideoType {
						mimeType = "video/mp4"
					}
					result.Data = &ParsedMediaData{idStr, p.Tags, img, p.CreationTime, mimeType}
				}
				ch <- result
			}(p)
		}
		// wait for all the goroutines to complete.
		wg.Wait()
		close(ch)
	}()
	return ch
}


// NewIGDownloadData loads downloaded Instagram data from the specified
// path and returns an instance of the data.
func NewIGDownloadData(dir, filename string) (*IGDownloadData, error) {
	igd := &IGDownloadData{baseIGDir: dir}
	if err := igd.load(filepath.Join(dir, filename)); err != nil {
		return nil, err
	}
	return igd, nil
}

// NewRecentData invokes the Instagram API to get recent media posted and
// returns an instance of the data.
func NewIGRecentData(igc *igclient.IGClient) (*IGRecentData, error) {
	data, err := igc.GetRecentMedia()
	if err != nil {
		return nil, err
	}
	glog.Infof("recent IG data: %v", string(data))
	var igrd IGRecentData
	if err := json.Unmarshal(data, &igrd); err != nil {
		return nil, err
	}
	return &igrd, nil
}
