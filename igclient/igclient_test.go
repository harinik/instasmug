package igclient

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"github.com/stretchr/testify/assert"
)

func createIGClient(cfgdir, tokenuri, mediauri string, client *http.Client) (*IGClient) {
	igccfg := IGClientConfig{
		ConfigDir: cfgdir,
		AccessTokenFile: "token.json",
		TokenEndpoint: tokenuri,
		MediaEndpoint: mediauri,
	}

	creds := IGCreds{
		ClientId: "client_id",
		ClientSecret: "sssh...secret",
	}

	at := IGAccessToken{
		Access_token: "",
	}
	return &IGClient{
		cachedToken: &at,
		httpClient: client,
		config: igccfg,
		creds: &creds,
	}
}

func TestGetTokenValidCode(t *testing.T) {
	cfgdir, err := ioutil.TempDir("/var/tmp", "igconfig")
	if err != nil {
		t.Errorf("error: %v creating temp dir", err)
	}
	defer os.RemoveAll(cfgdir)
	
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "{\"Access_token\": \"valid_code_presented\"}")
	}))
	defer ts.Close()

	igc := createIGClient(cfgdir, ts.URL, "", ts.Client())

	if err := igc.getToken("valid_code"); err != nil {
		t.Errorf("error: %v getting token", err)
	}
}

func TestGetTokenInvalidCode(t *testing.T) {
	cfgdir, err := ioutil.TempDir("/var/tmp", "igconfig")
	if err != nil {
		t.Errorf("error: %v creating temp dir", err)
	}
	defer os.RemoveAll(cfgdir)
	
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Invalid token", http.StatusForbidden)
	}))
	defer ts.Close()

	igc := createIGClient(cfgdir, ts.URL, "", ts.Client())

	if err := igc.getToken("invalid_code"); err == nil {
		t.Error("expected error getting token")
	}
}

func TestGetRecentMediaSuccess(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Recent media bytes")
	}))
	defer ts.Close()

	igc := createIGClient("", "", ts.URL, ts.Client())
	b, err := igc.GetRecentMedia()
	if err != nil {
		t.Errorf("error: %v when getting recent media", err)
	}
	assert.Equal(t, "Recent media bytes", string(b))
}

func TestGetRecentMediaFailure(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "invalid access token", http.StatusForbidden)
	}))
	defer ts.Close()

	igc := createIGClient("", "", ts.URL, ts.Client())
	if b, err := igc.GetRecentMedia(); err == nil {
		t.Error("expected error getting recent media")
	}
}
