// igclient is an Instagram client.
// It authenticates using OAuth v2.0 and uses the obtained access tokens
// to call the Instagram APIs.
// The access token is saved to a local file so that the auth calls do not
// have to be done each time.
// Author: Harini Krishnamurthy
package igclient

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"
	"github.com/golang/glog"
)

const (
	IGAuthEndpoint = "https://api.instagram.com/oauth/authorize/"
	IGTokenEndpoint = "https://api.instagram.com/oauth/access_token"
	IGMediaEndpoint = "https://api.instagram.com/v1/users/self/media/recent/"
	// NOTE: this is registered as the redirect URI in Instagram so any
	// changes to the host or port for the server during auth must be kept
	// in sync.
	redirectUri = "http://localhost:8000/instagram"
)

// IGCreds holds the Instagram client Id and secret.
type IGCreds struct {
	ClientId string
	ClientSecret string
}

// IGClientConfig defines the configuration parameters for the client.
type IGClientConfig struct {
	// Config dir where credentials are stored.
	ConfigDir string
	// The Credential file, relative to the config dir.
	CredsFile string
	// The AccessToken file, relative to the config dir.
	AccessTokenFile string

	// IG auth endpoint URI.
	AuthEndpoint string
	// IG token endpoint URI.
	TokenEndpoint string
	// IG media endpoint URI.
	MediaEndpoint string

	// Auth timeout.
	AuthTimeoutSeconds time.Duration
}

// IGAccessToken represents the access token.
// It can be marshaled to and unmarshaled from a JSON representation.
type IGAccessToken struct {
	Access_token string
}

// IGClient is the Instagram client that holds the authentication token
// and HTTP client.
type IGClient struct {
	// client ID and secret.
	creds *IGCreds

	// channel which is used to indicate if authentication is completed.
	// This can be set in one of two ways -
	// 1. by code that reads access token from the local file.
	// 2. by code that does the auth by calling IG APIs.
	authDone chan bool
	// cached access token to avoid the auth dance everytime.
	cachedToken *IGAccessToken
	// http client to make calls to the API endpoint.
	httpClient *http.Client

	// client config params
	config IGClientConfig
}

// igCallbackHandler is the handler for the redirect from Instagram
// with the verification code that is required for authentication.
func (igc *IGClient) igCallbackHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	// once the code is received, get the access token.
	if err:= igc.getToken(q.Get("code")); err != nil {
		// TODO: how to propagate errors better ?
		glog.Errorf("error getting access token: %v", err)
	} else {
		// signal that auth is complete.
		// set authDone to true in a separate goroutine so that this doesn't
		// block on the receiver of the channel being ready.
		go func() {
			igc.authDone <- true
		}()
	}
}

// readClientIdAndSecret reads the IG credentials from the file provided.
func readClientIdAndSecret(igconfigfile string) (*IGCreds, error) {
	f, err := os.Open(igconfigfile)
	if err != nil {
		return nil, fmt.Errorf("error: %v opening file: %v", err, igconfigfile)
	}
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	var creds IGCreds
	if err := json.Unmarshal(data, &creds); err != nil {
		return nil, err
	}
	return &creds, nil
}

// readAccessToken reads the access token from the provided file.
func readAccessToken(accessTokenFilePath string) (*IGAccessToken, error) {
	if _, err := os.Stat(accessTokenFilePath); !os.IsNotExist(err) {
		
		f, err := os.Open(accessTokenFilePath)
		if err != nil {
			return nil, err
		}
		defer f.Close()
		
		data, err := ioutil.ReadAll(f)
		if err != nil {
			return nil, err
		}
		var token IGAccessToken
		if err := json.Unmarshal(data, &token); err != nil {
			return nil, err
		}
		return &token, nil
	}
	return nil, fmt.Errorf("Access token file does not exist.")
}

// requestCode creates a URL that the user has to authorize, to get a code
// that can be exchanged for the access token. Right now, the URL is just
// printed out so that the user can click on it.
func (igc *IGClient) requestCode() {
	u, _ := url.Parse(igc.config.AuthEndpoint)
	q := u.Query()
	q.Set("client_id", igc.creds.ClientId)
	q.Set("redirect_uri", redirectUri)
	q.Set("response_type", "code")
	u.RawQuery = q.Encode()

	fmt.Printf("Login at URL: %v in your browser to authorize this app to use Instagram data\n", u.String())
}

// saveAccessToken saves the access token in JSON format to a file.
func (igc *IGClient) saveAccessToken() error {
	tokenBytes, err := json.Marshal(igc.cachedToken)
	if err != nil {
		return err
	} else {
		accessTokenPath := filepath.Join(igc.config.ConfigDir, igc.config.AccessTokenFile)
		if err := ioutil.WriteFile(accessTokenPath, tokenBytes, 0644); err != nil {
			return err
		}
	}
	return nil
}

// getToken exchanges the code sent by Instagram for an access token. It
// caches the access token in the client and also saves the token to a file
// for future use.
func (igc *IGClient) getToken(code string) error {
	if code != "" {
		form := url.Values{}
		form.Set("client_id", igc.creds.ClientId)
		form.Set("client_secret", igc.creds.ClientSecret)
		form.Set("grant_type", "authorization_code")
		form.Set("redirect_uri", redirectUri)
		form.Set("code", code)

		// post parameters as a form.
		resp, err := igc.httpClient.PostForm(igc.config.TokenEndpoint, form)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			return fmt.Errorf("got error code: %v", resp.Status)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("error: %v reading response body", err)
		}
		glog.Infof("getToken resp body: %v", string(body))
		var token IGAccessToken
		if err := json.Unmarshal(body, &token); err != nil {
			return err
		}
		igc.cachedToken = &token
		if err:= igc.saveAccessToken(); err != nil {
			return err
		}
	}
	return nil
}

// GetRecentMedia gets recent media for the user from Instagram.
func (igc *IGClient) GetRecentMedia() ([]byte, error) {
	u, _ := url.Parse(igc.config.MediaEndpoint)
	q := u.Query()
	q.Set("access_token", igc.cachedToken.Access_token)
	u.RawQuery = q.Encode()

	resp, err := igc.httpClient.Get(u.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("got error code: %v", resp.Status)
	}
	
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, err
}

// NewIGClient creates a new IGClient object using the default HTTP client.
func NewIGClient(igconfig IGClientConfig) (*IGClient, error) {
	return newClient(igconfig, http.DefaultClient)
}

// newClient creates a new IGClient object that has authenticated with
// Instagram and can be used to invoke APIs.
// The auth flow is as follows -
// 1. Attempt to read access token from the local file specified.
// 2. If the token is available, use that and signal that auth has completed.
// 3. If not, direct the user to authorize the app, get a verification code
// and then exchange that for an access token.
func newClient(igconfig IGClientConfig, client *http.Client) (*IGClient, error) {
	creds, err := readClientIdAndSecret(filepath.Join(igconfig.ConfigDir, igconfig.CredsFile))	
	if err != nil {
		return nil, err
	}
	igc := &IGClient{
		authDone: make(chan bool),
		creds: creds,
		httpClient: client,
		config: igconfig,
	}

	token, err := readAccessToken(filepath.Join(igc.config.ConfigDir, igc.config.AccessTokenFile))
	// if the access token is available from the file, cache it in the client
	// and signal that auth is complete.
	if err == nil {
		igc.cachedToken = token
		// set authDone to true in a separate goroutine so that this doesn't
		// block on the receiver of the channel being ready.
		go func() {
			igc.authDone <- true
		}()
	} else {
		// start a http server in a separate thread to handle the IG redirect
		// with the verification code.
		http.HandleFunc("/instagram", igc.igCallbackHandler)
		go func() {
			glog.Info("starting server listening on port 8000")
			http.ListenAndServe(":8000", nil)
		}()
		// direct the user to authorize the app.
		igc.requestCode()
	}
	// wait until auth is complete or it times out.
	select {
	case <-igc.authDone:
		glog.Info("IG auth is complete.")
		return igc, nil
	case <- time.After(igc.config.AuthTimeoutSeconds):
		glog.Error("IG authentication timed out.")
		return nil, fmt.Errorf("Timed out during auth")
	}
}
