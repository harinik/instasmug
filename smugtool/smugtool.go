// Implementation of a tool to upload Instagram data to Smugmug.
// Author: Harini Krishnamurthy

// This tool supports the upload of data from 2 sources:
// 1. the zip file that Instagram emails you when you request a data download
// 2. recent images downloaded by calling the /users/self/media/recent IG endpoint.
// For (1), it parses the metadata file (usually media.json) that lists all 
// metadata and uses the provided paths to read the image bytes.
// For (2), it parses the JSON response to a GET call on the endpoint
// /users/self/media/recent and fetches the media from the URLs specified
// in the JSON response.
// It then uploads each image to a Smugmug album. It is assumed that the
// Smugmug album is already created and that the albumKey is provided.
// Each uploaded image has a caption and keywords - the caption is the
// creation time of the image and the keywords are the hashtags retrieved
// from IG data.
// TODO: need better error handling.
package main

import (
	"flag"
	"sync"
	"time"
	"github.com/golang/glog"
	"gitlab.com/harinik/instasmug/igclient"
	"gitlab.com/harinik/instasmug/igdata"
	"gitlab.com/harinik/instasmug/smugclient"
)

var (
	// flag variables
	// smugmug API access
	smugTokenDir string
	smugOAuthTokenFile string
	smugApiTokenFile string
	smugAlbumKey string
	numUploaders int
	ignoreVideos bool

	// Instagram download data
	igDataDir string
	igMetadataFile string

	// Instagram API access
	igConfigDir string
	igCredFile string
	igAccessTokenFile string
	igAuthTimeoutSeconds time.Duration
)

func initFlags() {
	flag.StringVar(&smugTokenDir, "smug_token_dir", "", "Directory where relevant Smugmug auth tokens are stored.")
	flag.StringVar(&smugOAuthTokenFile, "smug_oauth_token_file", "oauth.token", "File to store oauth token, relative to the token directory.")
	flag.StringVar(&smugApiTokenFile, "smug_api_token_file", "", "File to read smugmug api token from, relative to the token directory.")
	flag.StringVar(&smugAlbumKey, "smug_album_key", "", "Smugmug album key that identifies the album to which the image is to be uploaded.")
	flag.IntVar(&numUploaders, "num_uploaders", 4, "Number of uploaders to use for uploading the images to Smugmug")
	flag.BoolVar(&ignoreVideos, "ignore_videos", true, "Ignore videos in the data. Defaults to true (for now, since Smugmug rejects the videos with an \"Unknown vide codec\" error).")
	flag.StringVar(&igDataDir, "ig_data_dir", "", "Directory where downloaded Instagram data is stored after unzipping.")
	flag.StringVar(&igMetadataFile, "ig_meta_file", "media.json", "Name of file yontaining metadata for downloaded Instagram data (usually media.json)")
	flag.StringVar(&igConfigDir, "ig_config_dir", "", "Dir that contains IG credentials")
	flag.StringVar(&igCredFile, "cred_file", "igcreds.json", "File that contains the Instagram API credentials in JSON format, relative to the config dir")
	flag.StringVar(&igAccessTokenFile, "ig_token_file", "token.json", "File to save the Instagram API access token in JSON format, relative to the config dir")
	flag.DurationVar(&igAuthTimeoutSeconds, "ig_auth_timeout", 10*time.Second, "Auth timeout in seconds")
	
	flag.Parse()
}

func main() {
	initFlags()
	processIGData()
	glog.Flush()
}

// processIGData processes downloaded Instagram data.
func processIGData() {
	// create a new Smugmug client
	smugcfg := defaultSmugClientConfig()
	sac, err := smugclient.NewSmugmugAuthClient(smugcfg)
	if err != nil {
		glog.Errorf("error %v creating smugmug auth client", err)
		return
	}
	
	// create an appropriate IGMediaFetcher depending on whether images from
	// the downloaded archive or recent images are being uploaded.
	var igd igdata.IGMediaFetcher
	if len(igDataDir) > 0 {
		// process downloaded Instagram data
		igd, err = igdata.NewIGDownloadData(igDataDir, igMetadataFile)
		if err != nil {
			glog.Errorf("error: %v loading IG data from file: %v", err, igMetadataFile)
			return
		}
	} else {
		igconfig := defaultIGClientConfig()
		// create an Instagram client
		igc, err := igclient.NewIGClient(igconfig)
		if err != nil {
			glog.Errorf("error: unable to create IG client: %v", err)
			return
		}

		// process recent data from Instagram
		igd, err = igdata.NewIGRecentData(igc)
		if err != nil {
			glog.Errorf("error: unable to get recent IG data: %v", err)
			return
		}
	}

	// get the channel returned by ImageIterator
	idc := igdata.ImageIterator(igd)
	var wg sync.WaitGroup
	wg.Add(numUploaders)
	for i := 0; i < numUploaders; i++ {
		go func() {
			defer wg.Done()
			// read from the channel and upload the image.
			// TODO: need better error handling
			uploadImage(sac, idc)
		}()
	}

	// wait until the upload is complete.
	wg.Wait()
}

// defaultSmugClientConfig returns the default Smugmug client config params.
func defaultSmugClientConfig() smugclient.SmugmugClientConfig {
	return smugclient.SmugmugClientConfig{
		TokenDir: smugTokenDir,
		ApiTokenFile: smugApiTokenFile,
		AccessTokenFile: smugOAuthTokenFile,
		AuthEndpoint: smugclient.SmugAuthEndpoint,
		RequestTokenEndpoint: smugclient.SmugRequestTokenEndpoint,
		AccessTokenEndpoint: smugclient.SmugAccessTokenEndpoint,
		ApiEndpoint: smugclient.SmugApiEndpoint,
		UploadEndpoint: smugclient.SmugUploadEndpoint,
		AlbumUri: smugclient.AlbumUri,
	}
}

// defaultIGClientConfig returns the default IG client config params.
func defaultIGClientConfig() igclient.IGClientConfig {
	return igclient.IGClientConfig{
		ConfigDir: igConfigDir,
		CredsFile: igCredFile,
		AccessTokenFile: igAccessTokenFile,
		AuthEndpoint: igclient.IGAuthEndpoint,
		TokenEndpoint: igclient.IGTokenEndpoint,
		MediaEndpoint: igclient.IGMediaEndpoint,
		AuthTimeoutSeconds: igAuthTimeoutSeconds,
	}
}

// uploadImage uploads the image to Smugmug
func uploadImage(sac *smugclient.SmugmugAuthClient, idc <-chan igdata.ImgDataResult) {
	for img := range idc {
		if img.Err != nil {
			glog.Errorf("error: %v reading image data from channel", img.Err)
		} else {
			keywords := append(img.Data.Hashtags, "instagram")
			glog.Infof("Photo Id: %v taken at: %v, hashtags: %v", img.Data.Id, img.Data.Date, keywords)
			if ignoreVideos && img.Data.MimeType == "video/mp4" {
				glog.Infof("Ignoring video id: %v taken at: %v", img.Data.Id, img.Data.Date)
				continue
			}
			if err := sac.Upload(
				smugAlbumKey,
				// X-Smug-FileName
				// img.Data.Id is set as the filename with the hope that
				// Smugmug will use it to de-dup uploads (it's not certain
				// at this point that it actually does - I'm waiting on a
				// response on this from the Smugmug API team).
				// Id is calculated based on the MD5 hash of the image and
				// the date taken.
				img.Data.Id,
				img.Data.MimeType,
				img.Data.Contents,
				// X-Smug-Caption
				// The date is set as the caption so that we can get ordering
				// in Smugmug's gallery view.
				// NOTE: The album is set up to reverse sort by caption.
				img.Data.Date,
				// X-Smug-Keywords
				keywords); err != nil {
				glog.Errorf("Error %v uploading file: %v to Smugmug", err, img.Data.Id)
			}
		}
	}
}
